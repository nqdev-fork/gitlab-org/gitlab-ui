import { shallowMount } from '@vue/test-utils';
import { BTableLite } from 'bootstrap-vue';
import GlTableLite from './table_lite.vue';

describe('GlTableLite', () => {
  let wrapper;

  const factory = (propsData) => {
    wrapper = shallowMount(GlTableLite, {
      propsData,
    });
  };

  const findBTableLite = () => wrapper.findComponent(BTableLite);

  it('adds gl-table class to tableClass prop', () => {
    factory({ tableClass: 'test-class' });

    expect(findBTableLite().props().tableClass).toEqual(['gl-table', 'test-class']);
  });

  it('adds gl-table fields to table prop', () => {
    const fields = ['name', 'age'];

    factory({ fields });

    expect(wrapper.props('fields')).toEqual(fields);
    expect(findBTableLite().props('fields')).toEqual(fields);
  });
});
